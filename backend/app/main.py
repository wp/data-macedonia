import pandas as pd
from fastapi import FastAPI
from sqlalchemy import text
from starlette.middleware.cors import CORSMiddleware

from .services.sessions import engine

app = FastAPI()

origins = [
    'http://localhost:3000'
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=['*'],
    allow_headers=['*']
)


@app.get('/years')
async def get_years():
    query = f"""
        select year as id, year as name
        from years;
    """
    df = pd.read_sql_query(query, engine)
    return df.to_dict(orient='records')


@app.get('/planning-regions')
async def get_municipalities():
    query = f"""
        select id, name
        from planning_regions
        order by id;
    """
    df = pd.read_sql_query(query, engine)
    return df.to_dict(orient='records')


@app.get('/municipalities')
async def get_municipalities():
    query = f"""
        select id, name
        from municipalities
        order by id;
    """
    df = pd.read_sql_query(query, engine)
    return df.to_dict(orient='records')


@app.get('/harmonized-classification/sections')
async def get_sections():
    query = f"""
        select id, name
        from section_codes
        order by id;
    """
    df = pd.read_sql_query(query, engine)
    return df.to_dict(orient='records')


@app.get('/harmonized-classification/hs2')
async def get_hs2_codes():
    query = f"""
        select id, description as name
        from hs2_codes
        order by id;
    """
    df = pd.read_sql_query(query, engine)
    return df.to_dict(orient='records')


@app.get('/harmonized-classification/hs4')
async def get_hs4_codes():
    query = f"""
        select (hs2_id || id) as id, description as name
        from hs4_codes
        order by (hs2_id || id);
    """
    df = pd.read_sql_query(query, engine)
    return df.to_dict(orient='records')


@app.get('/harmonized-classification/hs6')
async def get_hs6_codes():
    query = f"""
        select (hs2_id || hs4_id || id) as id, description as name
        from hs6_codes
        order by (hs2_id || hs4_id || id);
    """
    df = pd.read_sql_query(query, engine)
    return df.to_dict(orient='records')


@app.get('/export-records/by-planning-region-and-year')
async def get_export_records_by_planning_region_and_year(planning_region_ids: str, years: str):
    planning_region_ids = tuple(map(int, planning_region_ids.split(',')))
    years = tuple(map(int, years.split(',')))
    query = text(f"""
        select year, planning_region_id, planning_region, section_id, section, hs2_id, hs2_description, (hs2_id || hs4_id) as hs4_id, hs4_description, (hs2_id || hs4_id || hs6_id) as hs6_id, hs6_description, sum(trade_value) as trade_value
        from export_data
        where planning_region_id in :planning_regions_ids and year in :years
        group by year, planning_region_id, planning_region, section_id, section, hs2_id, hs2_description, hs4_id, hs4_description, hs6_id, hs6_description;
    """)
    df = pd.read_sql_query(query, engine, params={'planning_regions_ids': planning_region_ids, 'years': years})
    df['year'] = df['year'].apply(str)
    return df.to_dict(orient='records')


@app.get('/export-records/by-municipality-and-year')
async def get_export_records_by_municipality_and_year(municipality_ids: str, years: str):
    municipality_ids = tuple(map(int, municipality_ids.split(',')))
    years = tuple(map(int, years.split(',')))
    query = text(f"""
        select year, municipality_id, municipality, section_id, section, hs2_id, hs2_description, (hs2_id || hs4_id) as hs4_id, hs4_description, (hs2_id || hs4_id || hs6_id) as hs6_id, hs6_description, sum(trade_value) as trade_value
        from export_data
        where municipality_id in :municipality_ids and year in :years
        group by year, municipality_id, municipality, section_id, section, hs2_id, hs2_description, hs4_id, hs4_description, hs6_id, hs6_description;
    """)
    df = pd.read_sql_query(query, engine, params={'municipality_ids': municipality_ids, 'years': years})
    df['year'] = df['year'].apply(str)
    return df.to_dict(orient='records')


@app.get('/export-records/by-section-and-year')
async def get_export_records_by_section_and_year(section_ids: str, years: str):
    section_ids = tuple(map(int, section_ids.split(',')))
    years = tuple(map(int, years.split(',')))
    query = text(f"""
            select year, municipality, municipality_id, planning_region, planning_region_id, section, section_id, sum(trade_value) as trade_value
            from export_data
            where section_id in :section_ids and year in :years
            group by year, municipality, municipality_id, planning_region, planning_region_id, section, section_id;
        """)
    df = pd.read_sql_query(query, engine, params={'section_ids': section_ids, 'years': years})
    df['year'] = df['year'].apply(str)
    return df.to_dict(orient='records')


@app.get('/export-records/by-hs2-and-year')
async def get_export_records_by_hs2_and_year(hs2_ids: str, years: str):
    hs2_ids = tuple(hs2_ids.split(','))
    years = tuple(map(int, years.split(',')))
    query = text(f"""
            select year, municipality, municipality_id, planning_region, planning_region_id, hs2_id, hs2_description, sum(trade_value) as trade_value
            from export_data
            where hs2_id in :hs2_ids and year in :years
            group by year, municipality, municipality_id, planning_region, planning_region_id, hs2_id, hs2_description;
        """)
    df = pd.read_sql_query(query, engine, params={'hs2_ids': hs2_ids, 'years': years})
    df['year'] = df['year'].apply(str)
    return df.to_dict(orient='records')


@app.get('/export-records/by-hs4-and-year')
async def get_export_records_by_hs4_and_year(hs4_ids: str, years: str):
    hs4_ids = tuple(hs4_ids.split(','))
    years = tuple(map(int, years.split(',')))
    query = text(f"""
            select year, municipality, municipality_id, planning_region, planning_region_id, (hs2_id || hs4_id) as hs4_id, hs4_description, sum(trade_value) as trade_value
            from export_data
            where (hs2_id || hs4_id) in :hs4_ids and year in :years
            group by year, municipality, municipality_id, planning_region, planning_region_id, hs2_id, hs4_id, hs4_description;
        """)
    df = pd.read_sql_query(query, engine, params={'hs4_ids': hs4_ids, 'years': years})
    df['year'] = df['year'].apply(str)
    return df.to_dict(orient='records')


@app.get('/export-records/by-hs6-and-year')
async def get_export_records_by_hs6_and_year(hs6_ids: str, years: str):
    hs6_ids = tuple(hs6_ids.split(','))
    years = tuple(map(int, years.split(',')))
    query = text(f"""
            select year, municipality, municipality_id, planning_region, planning_region_id, (hs2_id || hs4_id || hs6_id) as hs6_id, hs6_description, sum(trade_value) as trade_value
            from export_data
            where (hs2_id || hs4_id || hs6_id) in :hs6_ids and year in :years
            group by year, municipality, municipality_id, planning_region, planning_region_id, hs2_id, hs4_id, hs6_id, hs6_description;
        """)
    df = pd.read_sql_query(query, engine, params={'hs6_ids': hs6_ids, 'years': years})
    df['year'] = df['year'].apply(str)
    return df.to_dict(orient='records')


@app.get('/export-records/by-planning-region-and-hs2')
async def get_export_records_by_planning_region_and_hs2(planning_region_ids: str):
    planning_region_ids = tuple(map(int, planning_region_ids.split(',')))
    query = text(f"""
                select year, section, section_id, hs2_id, hs2_description, sum(trade_value) as trade_value
                from export_data
                where planning_region_id in :planning_region_ids
                group by year, section, section_id, hs2_id, hs2_description;
            """)
    df = pd.read_sql_query(query, engine, params={'planning_region_ids': planning_region_ids})
    return df.to_dict(orient='records')


@app.get('/export-records/by-planning-region-and-hs4')
async def get_export_records_by_planning_region_and_hs4(planning_region_ids: str):
    planning_region_ids = tuple(map(int, planning_region_ids.split(',')))
    query = text(f"""
                select year, section, section_id, (hs2_id || hs4_id) as hs4_id, hs4_description, sum(trade_value) as trade_value
                from export_data
                where planning_region_id in :planning_region_ids
                group by year, section, section_id, hs2_id, hs4_id, hs4_description;
            """)
    df = pd.read_sql_query(query, engine, params={'planning_region_ids': planning_region_ids})
    return df.to_dict(orient='records')


@app.get('/export-records/by-planning-region-and-hs6')
async def get_export_records_by_planning_region_and_hs6(planning_region_ids: str):
    planning_region_ids = tuple(map(int, planning_region_ids.split(',')))
    query = text(f"""
                select year, section, section_id, (hs2_id || hs4_id || hs6_id) as hs6_id, hs6_description, sum(trade_value) as trade_value
                from export_data
                where planning_region_id in :planning_region_ids
                group by year, section, section_id, hs2_id, hs4_id, hs6_id, hs6_description;
            """)
    df = pd.read_sql_query(query, engine, params={'planning_region_ids': planning_region_ids})
    return df.to_dict(orient='records')


@app.get('/export-records/by-municipality-and-hs2')
async def get_export_records_by_municipality_and_hs2(municipality_ids: str):
    municipality_ids = tuple(map(int, municipality_ids.split(',')))
    query = text(f"""
                select year, section, section_id, hs2_id, hs2_description, sum(trade_value) as trade_value
                from export_data
                where municipality_id in :municipality_ids
                group by year, section, section_id, hs2_id, hs2_description;
            """)
    df = pd.read_sql_query(query, engine, params={'municipality_ids': municipality_ids})
    return df.to_dict(orient='records')


@app.get('/export-records/by-municipality-and-hs4')
async def get_export_records_by_municipality_and_hs4(municipality_ids: str):
    municipality_ids = tuple(map(int, municipality_ids.split(',')))
    query = text(f"""
                select year, section, section_id, (hs2_id || hs4_id) as hs4_id, hs4_description, sum(trade_value) as trade_value
                from export_data
                where municipality_id in :municipality_ids
                group by year, section, section_id, hs2_id, hs4_id, hs4_description;
            """)
    df = pd.read_sql_query(query, engine, params={'municipality_ids': municipality_ids})
    return df.to_dict(orient='records')


@app.get('/export-records/by-municipality-and-hs6')
async def get_export_records_by_municipality_and_hs6(municipality_ids: str):
    municipality_ids = tuple(map(int, municipality_ids.split(',')))
    query = text(f"""
                select year, section, section_id, (hs2_id || hs4_id || hs6_id) as hs6_id, hs6_description, sum(trade_value) as trade_value
                from export_data
                where municipality_id in :municipality_ids
                group by year, section, section_id, hs2_id, hs4_id, hs6_id, hs6_description;
            """)
    df = pd.read_sql_query(query, engine, params={'municipality_ids': municipality_ids})
    return df.to_dict(orient='records')


@app.get('/export-records/by-section')
async def get_export_records_by_section(section_ids: str):
    section_ids = tuple(section_ids.split(','))
    query = text(f"""
            select year, municipality, municipality_id, planning_region_id, planning_region, sum(trade_value) as trade_value
            from export_data
            where section_id in :section_ids
            group by year, municipality, municipality_id, planning_region_id, planning_region;
        """)
    df = pd.read_sql_query(query, engine, params={'section_ids': section_ids})
    df['year'] = df['year'].apply(str)
    return df.to_dict(orient='records')


@app.get('/export-records/by-hs2')
async def get_export_records_by_section(hs2_ids: str):
    hs2_ids = tuple(hs2_ids.split(','))
    query = text(f"""
            select year, municipality, municipality_id, planning_region_id, planning_region, sum(trade_value) as trade_value
            from export_data
            where hs2_id in :hs2_ids
            group by year, municipality, municipality_id, planning_region_id, planning_region;
        """)
    df = pd.read_sql_query(query, engine, params={'hs2_ids': hs2_ids})
    df['year'] = df['year'].apply(str)
    return df.to_dict(orient='records')


@app.get('/export-records/by-hs4')
async def get_export_records_by_section(hs4_ids: str):
    hs4_ids = tuple(hs4_ids.split(','))
    query = text(f"""
            select year, municipality, municipality_id, planning_region_id, planning_region, sum(trade_value) as trade_value
            from export_data
            where (hs2_id || hs4_id) in :hs4_ids
            group by year, municipality, municipality_id, planning_region_id, planning_region;
        """)
    df = pd.read_sql_query(query, engine, params={'hs4_ids': hs4_ids})
    df['year'] = df['year'].apply(str)
    return df.to_dict(orient='records')


@app.get('/export-records/by-hs6')
async def get_export_records_by_section(hs6_ids: str):
    hs6_ids = tuple(hs6_ids.split(','))
    query = text(f"""
            select year, municipality, municipality_id, planning_region_id, planning_region, sum(trade_value) as trade_value
            from export_data
            where (hs2_id || hs4_id || hs6_id) in :hs6_ids
            group by year, municipality, municipality_id, planning_region_id, planning_region;
        """)
    df = pd.read_sql_query(query, engine, params={'hs6_ids': hs6_ids})
    df['year'] = df['year'].apply(str)
    return df.to_dict(orient='records')
