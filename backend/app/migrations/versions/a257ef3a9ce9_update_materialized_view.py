"""Update materialized view

Revision ID: a257ef3a9ce9
Revises: 39492307e961
Create Date: 2024-01-21 21:42:13.762912

"""
from typing import Sequence, Union

from alembic import op

# revision identifiers, used by Alembic.
revision: str = 'a257ef3a9ce9'
down_revision: Union[str, None] = '39492307e961'
branch_labels: Union[str, Sequence[str], None] = None
depends_on: Union[str, Sequence[str], None] = None


def upgrade() -> None:
    op.execute("""
        refresh materialized view export_data
    """)


def downgrade() -> None:
    pass
