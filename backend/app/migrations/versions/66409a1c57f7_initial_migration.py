"""Initial migration

Revision ID: 66409a1c57f7
Revises: 
Create Date: 2024-01-20 15:20:49.730100

"""
from typing import Sequence, Union

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision: str = '66409a1c57f7'
down_revision: Union[str, None] = None
branch_labels: Union[str, Sequence[str], None] = None
depends_on: Union[str, Sequence[str], None] = None


def upgrade() -> None:
    op.execute(sa.DDL("""
        create table years
        (
            year integer not null primary key
        )
    """))

    op.execute(sa.DDL("""
        create table planning_regions
        (
            id   integer not null primary key,
            name varchar not null
        )
    """))

    op.execute(sa.DDL("""
        create table municipalities
        (
            id                 integer not null primary key,
            name               varchar not null,
            planning_region_id integer not null references planning_regions
        )
    """))

    op.execute(sa.DDL("""
        create table section_codes
        (
            id   integer not null primary key,
            name varchar not null
        )
    """))

    op.execute(sa.DDL("""
        create table hs2_codes
        (
            section_id  integer    not null references section_codes,
            id          varchar(2) not null,
            description text       not null,
            primary key (section_id, id)
        )
    """))

    op.execute(sa.DDL("""
    create table hs4_codes
        (
            section_id  integer    not null,
            hs2_id      varchar(2) not null,
            id          varchar(2) not null,
            description text       not null,
            primary key (section_id, hs2_id, id),
            foreign key (section_id, hs2_id) references hs2_codes
        )
    """))

    op.execute(sa.DDL("""
    create table hs6_codes
        (
            section_id  integer    not null,
            hs2_id      varchar(2) not null,
            hs4_id      varchar(2) not null,
            id          varchar(2) not null,
            description text       not null,
            primary key (section_id, hs2_id, hs4_id, id),
            foreign key (section_id, hs2_id, hs4_id) references hs4_codes
        )
    """))

    op.execute("""
        create view hs_classification (section_id, section_name, hs2_id, hs2_description, hs4_id, hs4_description, hs6_id, hs6_description) as
        SELECT sc.id                                          AS section_id,
               sc.name                                        AS section_name,
               hs2.id                                         AS hs2_id,
               hs2.description                                AS hs2_description,
               hs2.id || hs4.id                               AS hs4_id,
               hs4.description                                AS hs4_description,
               (hs2.id || hs4.id) || hs6.id                   AS hs6_id,
               hs6.description                                AS hs6_description
        FROM hs6_codes hs6
                 LEFT JOIN hs4_codes hs4 ON hs4.section_id = hs6.section_id AND hs4.hs2_id = hs6.hs2_id AND hs4.id = hs6.hs4_id
                 LEFT JOIN hs2_codes hs2 ON hs4.section_id = hs2.section_id AND hs4.hs2_id = hs2.id
                 LEFT JOIN section_codes sc ON hs2.section_id = sc.id
    """)

    op.execute(sa.DDL("""
        create table tariffs
        (
            section_id  integer    not null,
            hs2_id      varchar(2) not null,
            hs4_id      varchar(2) not null,
            hs6_id      varchar(2) not null,
            id          varchar(4) not null,
            description text,
            primary key (section_id, hs2_id, hs4_id, hs6_id, id),
            foreign key (section_id, hs2_id, hs4_id, hs6_id) references hs6_codes
        )
    """))

    op.execute(sa.DDL("""
        create table export_records
        (
            id                   bigserial primary key,
            year                 integer    not null references years,
            section_id           integer    not null,
            hs2_id               varchar(2) not null,
            hs4_id               varchar(2) not null,
            hs6_id               varchar(2) not null,
            tariff_discriminator varchar(4) not null,
            municipality_id      integer    not null references municipalities,
            trade_value          numeric    not null,
            foreign key (section_id, hs2_id, hs4_id, hs6_id, tariff_discriminator) references tariffs
        )
    """))

    op.execute("""
        create materialized view export_data as
        SELECT er.year,
               m.id            AS municipality_id,
               m.name          AS municipality,
               pr.id           AS planning_region_id,
               pr.name         AS planning_region,
               er.section_id   AS section_id,
               sc.name         AS section,
               er.hs2_id       AS hs2_id,
               hs2.description AS hs2_description,
               er.hs4_id       AS hs4_id,
               hs4.description AS hs4_description,
               er.hs6_id       AS hs6_id,
               hs6.description AS hs6_description,
               er.trade_value  AS trade_value
        FROM export_records er
                 LEFT JOIN hs6_codes hs6 ON hs6.id = er.hs6_id AND hs6.hs4_id = er.hs4_id AND
                                            hs6.hs2_id = er.hs2_id AND hs6.section_id = er.section_id
                 LEFT JOIN hs4_codes hs4 ON hs4.section_id = hs6.section_id AND hs4.hs2_id = hs6.hs2_id AND
                                            hs4.id = hs6.hs4_id
                 LEFT JOIN hs2_codes hs2 ON hs4.section_id = hs2.section_id AND hs4.hs2_id = hs2.id
                 LEFT JOIN section_codes sc ON hs2.section_id = sc.id
                 LEFT JOIN municipalities m ON er.municipality_id = m.id
                 LEFT JOIN planning_regions pr ON m.planning_region_id = pr.id
        ORDER BY er.year, m.name, sc.name, er.hs2_id, er.hs4_id, er.hs6_id
    """)


def downgrade() -> None:
    pass
