"""Populate tables

Revision ID: 39492307e961
Revises: 66409a1c57f7
Create Date: 2024-01-21 19:22:07.869019

"""
from typing import Sequence, Union

from alembic import op

# revision identifiers, used by Alembic.
revision: str = '39492307e961'
down_revision: Union[str, None] = '66409a1c57f7'
branch_labels: Union[str, Sequence[str], None] = None
depends_on: Union[str, Sequence[str], None] = None


def upgrade() -> None:
    with open('migrations/inserts.sql', 'r', encoding='utf-8') as file:
        for statement in file.read().split('\n'):
            if statement == "":
                continue

            op.execute(statement[:-1])


def downgrade() -> None:
    pass
