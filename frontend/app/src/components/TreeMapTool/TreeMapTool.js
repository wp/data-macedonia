import React from 'react';
import {Treemap} from "d3plus-react";
import {CircularProgress} from "@mui/material";


const TreeMapTool = ({config, loading = false}) => {
    return (
        <div className={"d-flex justify-content-center align-items-center h-100"}>
            {loading && <CircularProgress/>}
            {!loading && <Treemap config={config}/>}
        </div>
    );
};

export default TreeMapTool;